Description
-----------
This module adds AJAX support to Webform (3.0 and above).
As a first step it focusses to simply add form AJAX paging and submit.
Especially for multipage forms, this leads to faster loading (less data transfer),
better usability and finally smaller server load.

Requirements
------------
Drupal 8.x
Webform 8.x-5.0-beta11 or newer.

Installation
------------
1. Copy the entire webform_ajax directory the Drupal modules directory.

2. Login as an administrator. Enable the module in the "Administer" -> "Modules"

3. Create or edit a webform and check "AJAX mode" at admin/structure/webform/manage/%webform/settings.

Upgrading from previous versions
--------------------------------
There is no upgrade path from Drupal 6/7. You just have to set the "AJAX mode"
property on each webform.

Support
-------
Please use the issue queue for filing bugs with this module at
http://drupal.org/project/issues/webform_ajax

