<?php

namespace Drupal\webform_ajax\Controller;

use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Ajax\ReplaceCommand;
use Drupal\Core\Controller\ControllerBase;
use Drupal\webform\Entity\Webform;
use Drupal\webform\WebformInterface;

/**
 * Provides an AjaxResponse for the requested webform
 */
class WebformAjaxController extends ControllerBase {
	
	/**
	 * Returns an AjaxResponse with a replace command to replace the contents of
	 * the wrapper_id with a new requested webform
	 *
	 * @param WebformInterface $webform        	
	 * @param string $wrapper_id        	
	 */
	public function return_webform(WebformInterface $webform, $wrapper_id) {
		$response = new AjaxResponse();
		
		// Display the webform only if it is enabled. Otherwise, show messages.
		if ($webform->isOpen()) {
			$form = Webform::load($webform->get('id'));
			$output = \Drupal::entityManager()->getViewBuilder('webform')->view($form);
		} else {
			$output = array (
				'#type' => 'markup',
				'#markup' => t('The webform cannot be displayed.') 
			);
		}
		
		$response->addCommand(new ReplaceCommand('#' . $wrapper_id, $output));
		
		return $response;
	}
}